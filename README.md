# postfix-cs

Inspect Postfix message queue: list them, read, delete...

Perl, and curses-based UI.

Author: Craig Sanders ( http://taz.net.au/postfix/scripts/ ), I simply hacked it in order to have it run on a recent (2022) Linux distribution (Debian stable).
